#FROM alpine:3.5
FROM alpine

RUN echo "ipv6" >> /etc/modules

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories

#RUN echo "http://dl-1.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories; \
#    echo "http://dl-2.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories; \
#    echo "http://dl-3.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories; \
#    echo "http://dl-4.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories; \
#    echo "http://dl-5.alpinelinux.org/alpine/edge/main" >> /etc/apk/repositories

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

#RUN echo "http://dl-1.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories; \
#    echo "http://dl-2.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories; \
#    echo "http://dl-3.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories; \
#    echo "http://dl-4.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories; \
#    echo "http://dl-5.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories

RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories

#RUN apk add oath-toolkit-libpskc-2.6.2-r2 --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ --allow-untrusted


RUN apk --update add expect --no-cache --allow-untrusted

RUN apk --update add openconnect --no-cache  --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing --allow-untrusted
#RUN apk --update add openconnect --no-cache  --allow-untrusted

ADD entrypoint.sh /entrypoint.sh

HEALTHCHECK  --interval=10s --timeout=10s --start-period=10s \
  CMD /sbin/ifconfig tun0

CMD ["/entrypoint.sh"]
